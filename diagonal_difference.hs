-- This is a HackerRank problem
-- Given a square matrix, find the absolute difference between the sums of its diagonals

import Data.List (transpose)

main :: IO ()
main = do
    input <- getContents
    let inputStrings = lines input
        sideSize = read $ head inputStrings :: Int

        inputMatrix :: [[Int]]
        inputMatrix = map (map read . words) (tail inputStrings)

        diagonalA = extractDiagElems $ prepForDiagExtraction inputMatrix sideSize
        diagonalB = extractDiagElems $ prepForDiagExtraction (map reverse inputMatrix) sideSize
    print . abs . sum $ zipWith (-) diagonalA diagonalB

oneDiagonalElem :: ([Int], Int) -> Int
oneDiagonalElem (matrixRow, elemsToDrop) = head $ drop elemsToDrop matrixRow

prepForDiagExtraction :: [[Int]] -> Int -> [([Int], Int)]
prepForDiagExtraction inputMatrix sideSize = zip inputMatrix arrayElemsToDrop
  where
    arrayElemsToDrop = map (subtract 1) [1 .. sideSize]

extractDiagElems :: [([Int], Int)] -> [Int]
extractDiagElems preppedMatrixRows = map oneDiagonalElem preppedMatrixRows