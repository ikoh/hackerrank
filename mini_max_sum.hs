import Data.List (sort)

main :: IO ()
main = do
    rawInput <- getLine
    let processedInput :: [Integer]
        processedInput = map read $ words rawInput
        sortedInput = sort processedInput
    putStrLn . unwords $ map (show . sum) [init sortedInput, tail sortedInput]
