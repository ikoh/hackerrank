-- This is a HackerRank problem

main :: IO ()
main = do
    rawInput <- getContents
    let input = lines rawInput

        denominator :: Float
        denominator = fromIntegral . read $ head input

        processedArray :: [Int]
        processedArray = map read $ words (last input)

        output = map (show . (/ denominator)) (propertyCounts processedArray [1, -1, 0])
    putStrLn $ unlines output

getOccurrencesOfProperty :: [Int] -> Int -> Float
getOccurrencesOfProperty xs x = fromIntegral . length . filter (== x) $ map signum xs

propertyCounts :: [Int] -> [Int] -> [Float]
propertyCounts inputArray numberPropertyList = map (getOccurrencesOfProperty inputArray) numberPropertyList